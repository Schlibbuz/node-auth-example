const express = require('express');
const router = express.Router();

router.use('/api', require('./api'));

router.get('/', (req, res, next) => {
	return res.send('hello-home');
});

module.exports = router;
